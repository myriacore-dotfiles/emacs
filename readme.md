# MyriaCore's Emacs Dotfiles

**Warning:** Track this repository at your own risk! I may introduce
breaking changes without prior notice.

This repository contains my dotfiles & configurations for
emacs. Currently, among other things, I've added support for:

- Vector PDF rendering via [pdf-tools](https://github.com/politza/pdf-tools)
- Tabbed buffers and tab-groups via [centaur-tabs](https://github.com/ema2159/centaur-tabs)
- Sidebar with filetree via [neotree](https://github.com/jaypei/emacs-neotree)
- Icons via [all-the-icons](https://github.com/domtronn/all-the-icons.el)
- Pretty themes via [doom-themes](https://github.com/domtronn/all-the-icons.el)
- Pretty modeline via [doom-modeline](https://github.com/seagle0128/doom-modeline)
- Fira Code Ligature support via [ligature.el](https://github.com/mickeynp/ligature.el)
- Search and autocomplete via [Ivy / Counsel / Swiper](https://github.com/abo-abo/swiper)
- Vim bindings via [evil mode](https://github.com/emacs-evil/evil)

Recently, I've been using OCaml and C, so I've got a few modifications
for C and Tuareg (via [opam-user-setup.el](opam-user-setup.el)). For
OCaml, I use tuareg in general, but I prefer using
[ocp-indent](https://github.com/OCamlPro/ocp-indent) for indentation,
as it's more customize, and it behaves less funky when using monad
operators like bind, for example.

| Directory / File                             | Description                                                            |
| -------------------------------------------- | ---------------------------------------------------------------------- |
| [`init.el`](/init.el)                        | My init file. Contains most of the code for configurations and addons. |
| [`/lisp`](/lisp)                             | Contains code I've written myself                                      |
| [`/lisp/util.el`](/lisp/util.el)             | Contains some useful functions, shorthand, etc.                        |
| [`/lisp/surround-with.el`](/lisp/util.el)    | Contains my own surround-with interactive functions.                   |
| [`opam-user-setup.el`](/opam-user-setup.el)  | Contains the `opam user-setup install` configuration for my OCaml env. |

Currently, I've added explicit language support for:

- C & C-like languages
- Groovy
- Python
- Shell scripts
- OCaml
- HTML & HTML Templates
- Markdown
