(require 'package)
;; Source: https://melpa.org/#/getting-started
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There ar   [ State ]: EDITED, shown value does not take effect until you set or save it.
 this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
  ;; and `package-pinned-packages`. Most users will not need or want to do this.
  ;; (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  )
(package-initialize)

;; Setup Use Package & Quelpa Use Package
;; Source:
;;  - Quelpa: https://github.com/quelpa/quelpa-use-package#installation
(eval-when-compile
  ;; Following line is not needed if use-package.el is in ~/.emacs.d
  ;; (add-to-list 'load-path "<path where use-package is installed>")
  (require 'use-package))

(use-package use-package-ensure-system-package
  :ensure t)

(require 'quelpa-use-package)
(setq quelpa-checkout-melpa-p nil) ;; set quelpa to not fetch melpa at runtime

;; Setup common-lisp extensions & seq functions
(eval-when-compile (require 'cl) (require 'seq))

;; Setup load path w/ my stuff
(add-to-list 'load-path "~/.emacs.d/lisp")
(require 'util)

;; === GENERAL CUSTOMIZATION ===

(use-package files
  :demand t
  :config ;; Saner Backups (source: https://www.emacswiki.org/emacs/BackupDirectory#t)
  (setq backup-directory-alist `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t))))

;; Scratch file, to help remind me what's what :)
(setq initial-scratch-message "\
;; This buffer is for notes you don't want to save, and 
;; emacs lisp evaluation. If you want to create a file, 
;; visit that file with C-x C-f, then enter the text in 
;; that file's own buffer.
;;  
;; =================== KEYBINDINGS =====================
;; Cut:                                    C-w
;; Copy:                                   M-w
;; Yank/Paste:                             C-y 
;;     		                 	
;; Init & Grow rectagle left               M-S-<left>
;; Init & Grow rectagle right              M-S-<right>
;; Init & Grow rectagle up                 M-S-<up>
;; Init & Grow rectagle down               M-S-<down>
;;     		                 	
;; Delete current window:                  C-x 0
;; Delete other windows:                   C-x 1
;; Split current window horizontally:      C-x 2
;; Split current window vertically:        C-x 3
;;  
;; Move to left tab:                       C-c C-<left>
;; Move to right tab:                      C-c C-<right>
;; Move to upper tab:                      C-c C-<up>
;; Move to lower tab:                      C-c C-<down>
;;     		                 	
;; Move to left window:                    M-<left>
;; Move to right window:                   M-<right>
;; Move to upper window:                   M-<up>
;; Move to lower window:                   M-<down>
;;     		                 	
;; Make selected window taller:            C-x ^
;; Make selected window wider:             C-x }
;; Make selected window narrower:          C-x {
;;     		                 	
;; Make text larger:                       C-x C-+
;; Make text smaller:                      C-x C--
;; Make text normal sized:                 C-x C-0

")

;; Move between windows
(use-package windmove
  :bind
  (("M-<left>" . windmove-left)         ; move to left window
   ("M-<right>" . windmove-right)       ; move to right window
   ("M-<up>" . windmove-up)             ; move to upper window
   ("M-<down>" . windmove-down)))       ; move to lower window

;; === EDITOR BEHAVIOR ===

;; line numbers
(use-package display-line-numbers
  :hook (prog-mode . display-line-numbers-mode))

;; display ruler at 80 characters
(use-package display-fill-column-indicator
  :hook ((prog-mode text-mode markdown-mode) . display-fill-column-indicator-mode)
  :custom (fill-column 80 "fill to 80 characters"))

(transient-mark-mode t) ;; mark goes away when not in use :D

;; Vim bindings
(use-package evil
  :ensure t :demand t
  :custom (evil-mode 1 "Enable vim bindings")
  :config
  (evil-set-leader 'normal (kbd "<SPC>"))
  (evil-set-leader 'insert (kbd "C-`"))
  ;; (evil-ex-binding COMMAND-STR) will show you what function is being callled
  ;; when you type a : command
  (evil-define-key 'normal 'global
    (kbd "<leader>w") #'evil-write         ;; :w
    (kbd "<leader>W") #'evil-save-and-quit ;; :wqa
    (kbd "<leader>q") #'evil-quit          ;; :q
    (kbd "<leader>Q") #'evil-quit-all))    ;; :qa

;; vim's %
(use-package evil-matchit
  :after (evil)
  :config (global-evil-matchit-mode 1))

;; vim-style commenting
(use-package evil-nerd-commenter
  :after (evil)
  :ensure t :demand t ; won't load in time if we don't force it
  :bind ;; Emacs Keybindings
  (("M-;" . evilnc-comment-or-uncomment-lines)
   ("C-c l" . evilnc-quick-comment-or-uncomment-to-the-line)
   ("C-c c" . evilnc-copy-and-comment-lines)
   ("C-c p" . evilnc-comment-or-uncomment-paragraphs))
  :config
  ;; Vim key bindings
  (evil-define-key 'normal 'prog-mode-map
    (kbd "<leader>ci") 'evilnc-comment-or-uncomment-lines
    (kbd "<leader>cl") 'evilnc-quick-comment-or-uncomment-to-the-line
    (kbd "<leader>ll") 'evilnc-quick-comment-or-uncomment-to-the-line
    (kbd "<leader>cc") 'evilnc-copy-and-comment-lines
    (kbd "<leader>cp") 'evilnc-comment-or-uncomment-paragraphs
    (kbd "<leader>cr") 'comment-or-uncomment-region
    (kbd "<leader>cv") 'evilnc-toggle-invert-comment-line-by-line
    (kbd "<leader>.") 'evilnc-copy-and-comment-operator
    ; if you prefer backslash key
    (kbd "<leader>;") 'evilnc-comment-operator))

;; vim-surround
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

;; multiple cursors via vim-rectangle instructions
(use-package evil-mc
  :ensure t
  :custom
  (evil-mc-enable-bar-cursor t "Make cursors display as a ibeam when in insert mode")
  :hook (evil-normal-state-entry . evil-mc-undo-all-cursors)
  :config
  (global-evil-mc-mode  1) ;; enable
  (evil-define-key 'visual evil-mc-key-map
    "A" #'evil-mc-make-cursor-in-visual-selection-end
    "I" #'evil-mc-make-cursor-in-visual-selection-beg))

;; auto-insert matching pairs
(use-package elec-pair
  :demand t
  :config (electric-pair-mode 1))

;; Spell checking
(use-package flyspell
  :ensure t :demand t
  :ensure-system-package aspell
  :hook ((text-mode . flyspell-mode)
         (markdown-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode)))

;; Auto-delete selection after typing.
(use-package delsel
  :disabled ;; only really useful out of evil mode
  :config
  (delete-selection-mode 1))

;; Fira Code Font Ligatures
(use-package ligature
  :demand t
  :quelpa (ligature :fetcher github :repo "mickeynp/ligature.el")
  :config
  ;; Enable the www ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable ligatures in programming modes
  (let ((ligs '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
                ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
                "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
                "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
                "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
                "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
                "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
                "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
                "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
                "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%")))
    (ligature-set-ligatures 'prog-mode ligs)
    (ligature-set-ligatures 'markdown-mode ligs)
    (ligature-set-ligatures 'text-mode ligs))
  (global-ligature-mode 't)
  (set-face-attribute 'default nil :font "Fira Code"))

;; Emoji: 😄, 🤦, 🏴󠁧󠁢󠁳󠁣󠁴󠁿
;; Source: https://github.com/syl20bnr/spacemacs/issues/6654#issuecomment-234799438
(defun --set-emoji-font (frame)
  "Adjust the font settings of FRAME so Emacs can display emoji properly.
Note that Color Emojis only render on Emacs version 27 or greater."
  (set-fontset-font t 'symbol "Twemoji" frame)
  (set-fontset-font t 'symbol "Apple Color Emoji" frame 'append)
  (set-fontset-font t 'symbol "Noto Color Emoji" frame 'append)
  (set-fontset-font t 'symbol "Segoe UI Emoji" frame 'append)
  (set-fontset-font t 'symbol "Symbola" frame 'append))

;; For when Emacs is started in GUI mode:
(--set-emoji-font nil)
;; Hook for when a frame is created with emacsclient
;; see https://www.gnu.org/software/emacs/manual/html_node/elisp/Creating-Frames.html
(add-hook 'after-make-frame-functions '--set-emoji-font)

;; Editor Indentation
;; Source: https://stackoverflow.com/a/1819405
(use-package prog-mode
  :demand t
  :custom
  (indent-tabs-mode nil "Use spaces instead of tabs by default")
  (tab-width 2 "Make the tab character display as 2 spaces"))

;; Detect Indentation with in C dtrt-indent!
;; Sources:
;;  - https://emacs.stackexchange.com/a/32943
;;  - http://emacs-fu.blogspot.com/2008/12/auto-detecting-indentation-style.html
(use-package dtrt-indent
  :quelpa (dtrt-indent :fetcher github :repo "jscheid/dtrt-indent")
  :hook ((c-mode-common . dtrt-indent-mode)))

;; Function to Reload init File!
;; Source: https://stackoverflow.com/a/41881018
(defun reload-init-file ()
  "Reloads Emacs init file, typically ~/.emacs or ~/.emacs.d/init.el"
  (interactive)
  (load-file user-init-file))
(global-set-key (kbd "C-c C-l") 'reload-init-file)

;; Function to edit a file as root!
;; Source: https://stackoverflow.com/a/48051007
 (defun sudo-edit (&optional arg)
  "Edit currently visited file as root.

With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

;; Functions for word deletion (as opposed to word yank/kill)
(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument ARG, do this that many times."
  (interactive "p")
  (delete-region (point) (progn (forward-word arg) (point))))

(defun backward-delete-word (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument ARG, do this that many times."
  (interactive "p")
  (delete-word (- arg)))

;; Make C-<backspace> delete backwards (as opposed to killing backwards)
;;      C-<delete>    delete forwards (as opposed to killing forwards)
(global-set-key (kbd "C-<backspace>") 'backward-delete-word)
(global-set-key (kbd "C-<delete>") 'delete-word)

(use-package surround-with
  :demand t)

;; Better help dialogues
(use-package helpful
  :bind
  ;; Note that the built-in `describe-function' includes both functions
  ;; and macros. `helpful-function' is functions only, so we provide
  ;; `helpful-callable' as a drop-in replacement.
  ("C-h f" . helpful-callable)
  ("C-h v" . helpful-variable)
  ("C-h k" . helpful-key)

  ("C-c C-d" . helpful-at-point)
  ("C-h F" . helpful-function)
  ("C-h C" . helpful-command)
  :custom
  (counsel-describe-function-function #'helpful-callable
    "Use helpful when looking up functions")
  (counsel-describe-variable-function #'helpful-variable
    "Use helpful when looking up variables"))

;; Used for native code chunk editing in markdown mode
(use-package edit-indirect
  :ensure t
  :defines edit-indirect-mode-map
  :hook (edit-indirect-mode . edit-indirect-define-ex-cmds)
  :config
  (define-keys edit-indirect-mode-map
    [remap evil-write]          'edit-indirect-save
    [remap evil-quit]           'edit-indirect-abort
    [remap evil-save-and-quit]  'edit-indirect-commit
    [remap evil-save-and-close] 'edit-indirect-commit))

;; LSP - Language Server Protocol
(use-package lsp-mode
  :ensure t
  :custom (lsp-headerline-breadcrumb-enable nil
            "Don't overwrite headerline (centaur tabs is using that)")
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :commands lsp)
(use-package lsp-ui :ensure t :commands lsp-ui-mode)
(use-package lsp-ivy :ensure t :commands lsp-ivy-workspace-symbol)
(use-package dap-mode :ensure t)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; == LANGUAGE SETTINGS ==

;; == Markdown ==
(use-package markdown-mode
;; TODO: Add $$ as an electric pair
  :ensure t
  :after edit-indirect
  :mode (("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . gfm-mode)
         ("\\.rmd\\'" . gfm-mode))
  :custom
  (markdown-list-indent-width 2 "Set the markdown indent width to 2")
  (markdown-spaces-after-code-fence 0
    "Don't put spaces between a codeblock and the name")
  (markdown-indent-on-enter 'indent-and-new-item
    "Indent and continue lists when the user presses enter")
  (markdown-make-gfm-checkboxes-buttons t
    "Allow clickable checkbox buttons")
  ;; markdown-hide-urls
  :init (setq markdown-command
              ;; TODO: adapt to:
              ;; https://gist.github.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf
              (concat 
               "pandoc"
               " --from gfm --to html --standalone"
               " --mathjax"
               " --template=Github.html5"
               ;; " --highlight-style=pygments")
               ))
  :config
  (setq markdown-live-preview-delete-export 'delete-on-destroy)
  (setq markdown-fontify-code-blocks-natively t)
  ;; Support gitlab's math blocks
  (add-to-list 'markdown-code-lang-modes '("math" . latex-mode)))

;; == Elisp ==
(use-package elisp-mode
  :demand t
  :hook (emacs-lisp-mode .
          (lambda () (setq-local prettify-symbols-alist
                            elisp-prettify-symbols-alist)))
  ;; The below usage isn't working for whatever reason
  ;; :config
  ;; (setq-mode-local emacs-lisp-mode
  ;;                  prettify-symbols-alist elisp-prettify-symbols-alist)
  :preface
  (defconst elisp-prettify-symbols-alist
    (append lisp-prettify-symbols-alist
            '(("lambdai" . (?λ (Br . Bl) ?i))
              ("lambdap" . (?λ (Br . Bl) ?p))))
    "Alist of symbol/\"pretty\" characters to be displayed when in
`emacs-lisp-mode'. 

Mainly adds support for my lambda macros, such as `lambdap' and `lambdai'.  

See `reference-point-alist' and the third argument of `compose-region' to
customize."))

;; lisp repl & system shell
(use-package eshell
  :ensure t
  :defines (eshell-visual-commands eshell-visual-subcommands)
  :config
  (setenv "EDITOR" "nvim") ;; set EDITOR var to vim
  (setq eshell-prompt-regexp "^[^#$\n]*[#$] "
        eshell-prompt-function 'bash-like-prompt-fn
        eshell-destroy-buffer-when-process-dies t)
   ;; useful for piping compilation buffers to files :D
  (add-hook 'eshell-mode-hook 'eshell-keybinding-mode-hook)
  ;; Customize visual commands
  (with-eval-after-load "em-term" ;; Source: https://emacs.stackexchange.com/q/34137
    (setq eshell-visual-commands (append '("gdb" "utop" "swipl") eshell-visual-commands))
    (add-to-list 'eshell-visual-subcommands '("git" "log" "diff" "show" "help"))
    (add-to-list 'eshell-visual-subcommands '("dune" "utop"))
    (add-to-list 'eshell-visual-options '("git" "--help" "--paginate")))
  :preface
  (defun eshell/cat-buf (buf-name)
    "Prints the buffer #<buffer `buf-name'> to the screen."
    (eshell/printnl (with-current-buffer buf-name (buffer-string))))
  (defun bash-like-prompt-fn nil ;; Source: https://www.emacswiki.org/emacs/EshellPrompt#toc6
    "Prompt function for eshell in the style of bash/zsh"
    (concat
	   "[" (user-login-name) "@" (system-name) " "
     (propertize
      (if (string= (eshell/pwd) (getenv "HOME"))
          "~" (eshell/basename (eshell/pwd)))
      'face `(:foreground "white"))
     "]"
	   (if (= (user-uid) 0) "# " "$ ")))
  (defun eshell-keybinding-mode-hook ()
    (define-key eshell-mode-map (kbd "<home>") 'eshell-bol)
    (define-key eshell-mode-map (kbd "<end>") 'end-of-line)
    (define-key eshell-mode-map (kbd "C-<home>")
      (lambda () (interactive) (goto-char eshell-last-output-end)))))

;; == BASH, SHELLS, & SCRIPTING ==

(use-package shell
  :custom (sh-basic-offset 2 "Default Indentation for shells should be 2"))

(use-package python
  :mode "\\.py'"
  :interpreter "python"
  :custom (python-indent-offset 2 "Set default indent depth for python to 2"))

(use-package groovy-mode
  :ensure t
  :mode "\\.groovy'"
  :interpreter "groovysh"
  :custom (groovy-indent-offset 2 "Set indent depth to 2."))

;; == C-Like Languages ==
;; make case statements indent inside switch blocks
(use-package cc-mode
  :custom (c-basic-offset 2 "Set indent depth to 2 for c")
  :config (c-set-offset 'case-label '+))

;; == Erlang ==
(use-package erlang
  :ensure t)

(use-package promela-mode
  :demand t
  :quelpa (promela-mode :fetcher github :repo "rudi/promela-mode"))

;; == Haskell & associated langs ==

(use-package dhall-mode
  :custom (dhall-use-header-line nil
             "Don't overwrite the headerline (centaur tabs is using that)"))

;; == HTML & Web Dev ==

(use-package web-mode
  :hook html-mode
  :custom
  (web-mode-enable-engine-detection t "Enables engine detection with the -*- engine: ENGINE -*- directive")
  (web-mode-code-indent-offset 2 "Code (javascript, php, etc.) indentation level")
  (web-mode-markup-indentation-offset 2 "HTML Markup Indentation Depth")
  (web-mode-markup-indent-offset 2 "HTML Markup Indent Depth"))

(use-package css-mode
  :custom (css-indent-offset 2 "CSS Indentation level should be 2"))

(use-package js
  :hook (js-mode . lsp)
  :custom (js-indent-level 2 "JS indentation offset should be 2 spaces"))

;; LSP debuggers for nodejs and browser js in firefox & chrome
(use-package dap-chrome
  :config (dap-chrome-setup))
(use-package dap-firefox
  :config (dap-firefox-setup))
(use-package dap-node
  :config (dap-node-setup))

;; === PACKAGE CUSTOMIZATION & INITIALIZATION ===

;; xclip mode to allow emacs to interact with my clipboard
(use-package xclip
  :ensure t :demand t
  :config (xclip-mode 1))

;; Ivy & Counsel
(use-package ivy
  :ensure t :demand t
  :config 
  (ivy-mode 1)
  (counsel-mode 1))

(use-package swiper
  :ensure t
  :bind ("C-s" . swiper-thing-at-point))

(use-package posframe
  :ensure t)

(use-package ivy-posframe
  :after (posframe)
  :ensure t :demand t
  :config
  (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-top-center)))
  (setq ivy-posframe-parameters
        '((left-fringe . 8)
          (right-fringe . 8)))
  (ivy-posframe-mode 1))

;; Buffer Tabs
(use-package centaur-tabs
  :ensure t :demand t
  :hook
  (dashboard-mode . centaur-tabs-local-mode)
  ;; (term-mode . centaur-tabs-local-mode)
  ;; (calendar-mode . centaur-tabs-local-mode)
  ;; (org-agenda-mode . centaur-tabs-local-mode)
  ;; (helpful-mode . centaur-tabs-local-mode)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward)
  ;; :custom-face
  ;; (centaur-tabs-active-bar-face ((t (:background "SkyBlue2"))))
  :custom
  (centaur-tabs-style "chamfer" "use tabs with chamfered edges")
  (centaur-tabs-set-icons t "enable icons")
  (centaur-tabs-set-height 32 "set tab height to 32 pixels")
  (centaur-tabs-set-bar 'under "Add underline below active tab")
  (x-underline-at-descrent-line t "Add underline below active tab")
  (centaur-tabs-mode t "Enable Centaur Tabs Mode")
  :config (centaur-tabs-headline-match) ;; Make headline use `centaur-tabs-default-face'
  ;; :preface ;; not working???
  ;; TODO: consider moving some of these fcns elsewhere, or at *least*
  ;;       refactoring things a little.
  ;; Source: https://github.com/ema2159/centaur-tabs#buffer-groups
  (defun centaur-tabs-buffer-groups ()
    "`centaur-tabs-buffer-groups' control buffers' group rules. Group
centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode'
`dired-mode' `org-mode' `magit-mode'.
In General:
- Buffers that are meant to be used for mostly viewing alongside code will 
  group to \"Passive Utility\".
- Buffers that are meant to be used for text input alongside code will group
  to \"Active Utility\".
- Other buffers that correspond to treemacs projects will be grouped by 
  `centaur-tabs-get-group-name'."
    (list
     (cond
      ((or (string-prefix-p "*compilation*" (buffer-name)) ;; will capture compilation & scratch
           (string-prefix-p "*Man" (buffer-name))
           (memq major-mode '(eww-mode
                              pdf-view-mode
                              doc-view-mode
                              image-mode
                              markdown-view-mode
                              markdown-live-preview-mode
                              help-mode
                              helpful-mode
                              apropos-mode
                              man-mode)))
       "Passive Utility")
      ((or (string-prefix-p "*scratch*" (buffer-name))
           (string-prefix-p "*eshell*" (buffer-name))
           (derived-mode-p 'shell-mode)
           (derived-mode-p 'term-mode)
           (memq major-mode '(utop-mode)) ;; repls
           (memq major-mode '(magit-process-mode
                              magit-status-mode
                              magit-diff-mode
                              magit-log-mode
                              magit-file-mode
                              magit-blob-mode
                              magit-blame-mode)))
       "Active Utility")
      ((derived-mode-p 'dired-mode)
       "Dired")
      ((string-prefix-p "*" (buffer-name))
       "Emacs")
      ((memq major-mode '(org-mode
                          org-agenda-clockreport-mode
                          org-src-mode
                          org-agenda-mode
                          org-beamer-mode
                          org-indent-mode
                          org-bullets-mode
                          org-cdlatex-mode
                          org-agenda-log-mode
                          diary-mode))
       "OrgMode")
      (t
       ;;(treemacs--select-workspace-by-name
        ;;(treemacs-workspace->name (treemacs--find-workspace (buffer-file-name))))
       ;;(treemacs-do-switch-workspace)
       
       (centaur-tabs-get-group-name (current-buffer))))))
  
  (defun centaur-tabs-hide-tab (x)
    "`centaur-tabs-hide-tab' controls whether buffer X should be shown or hidden
 when in `centaur-tabs-mode'. Returns true if buffer X should be hidden; false
 otherwise.

Hides tab X if:
- Buffer starts with *epc
- Buffer starts with *helm
- Buffer starts with *Compile-Log*
- Buffer starts with *lsp
- Buffer starts with *company
- Buffer starts with *Flycheck
- Buffer starts with *tramp
- Buffer starts with *Mini
- Buffer starts with *straight
- Buffer starts with *temp
- Buffer starts with *dashboard*"
    (let ((name (format "%s" x)))
      (or
       ;; Current window is not dedicated window.
       (window-dedicated-p (selected-window))
       
       ;; Buffer name not match below blacklist.
       (string-prefix-p "*epc" name)
       (string-prefix-p "*helm" name)
       (string-prefix-p "*Compile-Log*" name)
       (string-prefix-p "*lsp" name)
       (string-prefix-p "*company" name)
       (string-prefix-p "*Flycheck" name)
       (string-prefix-p "*tramp" name)
       (string-prefix-p " *Mini" name)
       ;; (string-prefix-p "*help" name)
       (string-prefix-p "*straight" name)
       (string-prefix-p " *temp" name)
       ;; (string-prefix-p "*Help" name)
       (string-prefix-p "*dashboard*" name)
       
       ;; Is not magit buffer.
       (and (string-prefix-p "magit" name)
            (not (file-name-extension name)))
       ))))

;; Better Startup Page
(use-package dashboard
  :ensure t :demand t
  :config
  ;; If a command line argument that doesn't refer to a directory is provided,
  ;; don't display dashboard at startup.
  (when (or (< (length command-line-args) 2) (file-directory-p (nth 1 command-line-args)))
    (add-hook 'after-init-hook (lambda () ;; Display useful lists of items
                                 (dashboard-insert-startupify-lists)))
    (add-hook 'emacs-startup-hook (lambda () ;; switch to dashboard buffer on startup
                                    (switch-to-buffer "*dashboard*")
                                    (goto-char (point-min))
                                    (redisplay))))
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

;; Highlighting of TODOs
(use-package fic-mode
  :ensure t
  :hook (prog-mode . fic-mode)
  :custom
  (fic-highlighted-words '("TODO" "todo" "Todo"             ;; single word variants
                           "TO DO" "to do" "To do" "To Do") ;; double word variants
                         "Set highlighted words")
  :custom-face
  (fic-face ((t (:foreground "orange" :underline t)))))

;; filetree sidebar
(use-package neotree
  :ensure t :demand t ;; Required, otherwise won't load in time
  :bind ([f8] . neotree-toggle)
  :config
  ;; (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  ;; (setq neo-theme 'icons)
  ;; Source: https://www.emacswiki.org/emacs/NeoTree#toc12 
  (evil-define-key 'normal neotree-mode-map (kbd "TAB") 'neotree-enter)
  (evil-define-key 'normal neotree-mode-map (kbd "SPC") 'neotree-quick-look)
  (evil-define-key 'normal neotree-mode-map (kbd "q") 'neotree-hide)
  (evil-define-key 'normal neotree-mode-map (kbd "RET") 'neotree-enter)
  (evil-define-key 'normal neotree-mode-map (kbd "g") 'neotree-refresh)
  (evil-define-key 'normal neotree-mode-map (kbd "n") 'neotree-next-line)
  (evil-define-key 'normal neotree-mode-map (kbd "p") 'neotree-previous-line)
  (evil-define-key 'normal neotree-mode-map (kbd "A") 'neotree-stretch-toggle)
  (evil-define-key 'normal neotree-mode-map (kbd "H") 'neotree-hidden-file-toggle)
  (evil-define-key 'normal neotree-mode-map (kbd "R") 'neotree-refresh)
  ;; Set neotree-dir as the find-directory function
  ;; Source: https://gitlab.com/myriacore-dotfiles/emacs/-/issues/15#note_341354830
  (setq find-directory-functions '((lambda (d) (neotree-dir d) (funcall initial-buffer-choice))))
  ;; See the following links for information about why the below function is here.
  ;; - https://gitlab.com/myriacore-dotfiles/emacs/-/merge_requests/1#note_349540899
  ;; - https://gitlab.com/myriacore-dotfiles/emacs/-/merge_requests/1#bugs-to-squash
  ;; - https://gitlab.com/myriacore-dotfiles/emacs/-/issues/15#note_349525417
  (add-hook 'emacs-startup-hook 'delete-find-directory-window)
  :preface
  (defun delete-find-directory-window ()
    "Deletes the extra window resulting from the
`find-directory-functions' running on startup."
    (when (and (>= (length command-line-args) 2)
               (file-directory-p (nth 1 command-line-args)))
      (select-window (get-buffer-window (funcall initial-buffer-choice)))
      (delete-other-windows))))


(use-package pdf-tools
  :ensure t
  :custom
  (pdf-view-continuous t "Make PDFView allow the user to scroll between pages continuously")
  (pdf-view-incompatible-modes '(display-line-numbers-mode)
                               "Mark `display-line-numbers' as incompatible with PDFView")
  :config
  (pdf-loader-install))

;; Xwidget browser
;; Source: https://www.reddit.com/r/emacs/comments/4srze9/watching_youtube_inside_emacs_25/?utm_source=share&utm_medium=web2x
(use-package xwidget
  :ensure t
  :bind
  (:map xwidget-webkit-mode-map ;; make these keys behave like a normal browser
   ([mouse-4] . xwidget-webkit-scroll-down)
   ([mouse-5] . xwidget-webkit-scroll-up)
   ("<up>" . xwidget-webkit-scroll-down)
   ("<down>" . xwidget-webkit-scroll-up)
   ("M-w" . xwidget-webkit-copy-selection-as-kill)
   ("M-d" . xwidget-webkit-browse-url)
   ("C-r" . xwidget-webkit-reload)
   ("C--" . xwidget-webkit-zoom-out)
   ("C-=" . xwidget-webkit-zoom-in ))
  :config
  ;; Make EVIL mode play nice with our keybindings
  (evil-define-key 'normal xwidget-webkit-mode (kbd [mouse-4]) 'xwidget-webkit-scroll-down)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd [mouse-5]) 'xwidget-webkit-scroll-up)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "<up>") 'xwidget-webkit-scroll-down)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "<down>") 'xwidget-webkit-scroll-up)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "M-w") 'xwidget-copy-selection-as-kill)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "M-d") 'xwidget-webkit-browse-url)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "C-r") 'xwidget-webkit-browse-url)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "C--") 'xwidget-webkit-zoom-out)
  (evil-define-key 'normal xwidget-webkit-mode-map (kbd "C-=") 'xwidget-webkit-zoom-in)
  ;; adapt webkit according to window configuration chagne automatically
  ;; without this hook, every time you change your window configuration,
  ;; you must press 'a' to adapt webkit content to new window size
  (add-hook 'window-configuration-change-hook
            (lambda ()
			        (when (equal major-mode 'xwidget-webkit-mode)
			          (xwidget-webkit-adjust-size-dispatch))))
  :preface
  (defun xwidget-webkit-browse-url-no-reuse (url &optional session)
    "By default, `xwidget-browse-url' will reuse a previous xwidget window, which will
override the current website, unless a prefix argument is supplied.

`xwidget-webkit-browse-url-no-reuse' will always open a new website in a new window."
    (interactive
     (progn
       (require 'browse-url)
       (browse-url-interactive-arg "xwidget-webkit URL: ")))
    (xwidget-webkit-browse-url url t))

  ;; make xwidget default browser
  (setq browse-url-browser-function
        (lambda (url session)
				  (other-window 1)
				  (xwidget-webkit-browse-url-no-reuse url))))


;; === PACKAGE THEMES ===

(when window-system
  ;; Doom themes
  (use-package doom-themes
    :ensure t
    :custom
    (doom-themes-neotree-file-icons 'icons "Enables all-the-icons in neotree")
    :config
    ;; Global settings (defaults)
    (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
          doom-themes-enable-italic t) ; if nil, italics is universally disabled
    (lexical-let ((theme 'doom-moonlight))
      ;; ((theme 'doom-snazzy))
      ;; ((theme 'doom-tommorow-night))
      ;; ((theme 'doom-palenight))
      ;; ((theme 'doom-iosvkem))
      ;; ((theme 'doom-horizon))
      ;; ((theme 'doom-city-lights))
      ;; ((theme 'doom-acario-dark))
      ;; inane setup so themes work in the daemon
      (if (daemonp)
          (add-hook 'after-make-frame-functions
                    (lambda (frame)
                      (with-selected-frame frame
                        (load-theme theme t)))))
      (load-theme theme t))
    
    ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)
    
    ;; Enable custom neotree theme (all-the-icons must be installed!)
    (doom-themes-neotree-config)
    
    ;; or for treemacs users
    ;; (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
    ;; (doom-themes-treemacs-config)
    
    ;; Corrects (and improves) org-mode's native fontification.
    (doom-themes-org-config))

  ;; Doom Modeline
  (use-package doom-modeline
    :ensure t
    :init (doom-modeline-mode 1)
    :custom
    (doom-modeline-icon t "Display Graphical Icons")
    (doom-modelin-indent-info t "Display indentation info"))

  ;; Set window title
  (setq frame-title-format '("" "%b @ %F " emacs-version)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(c-basic-offset 2)
 '(centaur-tabs-mode t nil (centaur-tabs))
 '(centaur-tabs-set-bar 'under)
 '(centaur-tabs-set-height 32 t)
 '(centaur-tabs-set-icons t)
 '(centaur-tabs-style "chamfer")
 '(column-number-mode t)
 '(counsel-describe-function-function 'helpful-callable)
 '(counsel-describe-variable-function 'helpful-variable)
 '(css-indent-offset 2)
 '(doom-modelin-indent-info t t)
 '(doom-modeline-icon t)
 '(doom-themes-neotree-file-icons 'icons)
 '(evil-mc-enable-bar-cursor t t)
 '(evil-mode 1)
 '(fic-highlighted-words '("TODO" "todo" "Todo" "TO DO" "to do" "To do" "To Do"))
 '(fill-column 80)
 '(groovy-indent-offset 2 t)
 '(indent-tabs-mode nil)
 '(js-indent-level 2)
 '(menu-bar-mode nil)
 '(package-selected-packages
   '(dap-node dap-firefox gdscript-mode shm dockerfile-mode sql-indent dhall-mode erlang ess groovy-mode haskell-mode web-mode evil-surround evil-mc multiple-cursors evil-nerd-commenter reason-mode neotree evil-matchit doom auctex yaml-mode evil zygospore pdf-tools dtrt-indent quelpa-use-package quelpa mmm-mode shr-tag-pre-highlight ivy-posframe posframe all-the-icons doom-themes shx xclip use-package tuareg markdown-mode dashboard counsel centaur-tabs))
 '(pdf-view-continuous t)
 '(pdf-view-incompatible-modes '(display-line-numbers-mode))
 '(python-indent-offset 2 t)
 '(scroll-bar-mode nil)
 '(sh-basic-offset 2 t)
 '(tab-width 2)
 '(tool-bar-mode nil)
 '(web-mode-code-indent-offset 2 t)
 '(web-mode-enable-engine-detection t t)
 '(web-mode-markup-indent-offset 2 t)
 '(web-mode-markup-indentation-offset 2 t)
 '(x-underline-at-descrent-line t t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fic-face ((t (:foreground "orange" :underline t)))))
