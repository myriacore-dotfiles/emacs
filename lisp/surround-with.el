;; Source: https://emacs.stackexchange.com/a/3500
(defun surround-with (text-begin text-end)
  "Surround current word or region with given text."
  (interactive "sStart tag: \nsEnd tag: ")
  (let (pos1 pos2 bds)
    (if (and transient-mark-mode mark-active)
        (let ((p1 (region-beginning))
              (p2 (region-end)))
          (progn
            (goto-char p1)
            (insert text-begin)
            (goto-char p2)
            (forward-char (length text-begin))
            (insert text-end)))
      (progn
        (setq bds (bounds-of-thing-at-point 'symbol))
        (goto-char (cdr bds))
        (insert text-end)
        (goto-char (car bds))
        (insert text-begin)))))

(defun surround-with-kbd ()
  "Surround current word or region with the '<kbd>' tag."
  (interactive)
  (surround-with "<kbd>" "</kbd>"))

(defun surround-with-table ()
  "Surround current word or region with the '<table>' tag."
  (interactive)
  (surround-with "<table>\n<td>\n" "\n</td>\n</table>"))

(defun surround-with-table-row ()
  "Surround current word or region with the '<tr>' tag."
  (interactive)
  (surround-with "<tr>\n" "\n</tr>"))

(defun surround-with-table-cell ()
  "Surround current word or region with the '<td>' tag."
  (interactive)
  (surround-with "<td>\n" "\n</td>"))

(defun surround-with-details (summary)
  "Surround current word or region with the '<details>' tag. Also ensures
a '<summary>' tag, given by SUMMARY."
  (interactive "sSummary: ")
  (surround-with (concat "<details>\n<summary>" summary "</summary>\n\n") "\n</details>"))

(provide 'surround-with)
