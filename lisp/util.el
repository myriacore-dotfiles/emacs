;; Used to define some functions that emacs lisp doesn't seem to have by default, but
;; definitely help make code more concise and a bit happier. 

;; MAP WITH INDEX

(defun mapcari (fun sequence)
  "Apply function FUN to the index and value of each element in SEQUENCE, 
and make a list of the results. 
The resulting list is just as long as SEQUENCE. SEQUENCE may be a list, a
vector, a bool-vector, or a string.

Exactly like `mapcar', but provides the lambda with the index of the current
element."
  (let ((idx -1)) ;; start @ -1 because we'll increment immediately to 0
    (mapcar (lambda (s) (setq idx (1+ idx)) (funcall fun idx s))
            sequence)))

(defun mapcani (fun sequence)
  "Apply function FUN to the index and value of each element in SEQUENCE, 
and concatenate the results by altering them (using ‘nconc’).
SEQUENCE may be a list, a vector, a bool-vector, or a string.

Exactly like `mapcan', but provides the lambda with the index of the current
element."
  (let ((idx -1)) ;; start @ -1 because we'll increment immediately to 0
    (mapcan (lambda (s) (setq idx (1+ idx)) (funcall fun idx s))
             sequence)))

(defmacro define-keys (keymap &rest binding-forms)
  "Creates multiple `define-key' bindings in KEYMAP using BINDING-FORMS. A
binding form is a key or a value. The expectation is that the keys are provided
first, then the values. For example:

(define-keys my-favorite-mode-map
  [remap their-command]       'my-command
  [remap their-other-command] 'my-other-command
  (kbd \"C-c C-s\")             'my-save-command)

... is equivalent to:

(progn
  (define-key my-favorite-mode-map
    [remap their-command]       'my-command)
  (define-key my-favorite-mode-map
    [remap their-other-command] 'my-other-command)
  (define-key my-favorite-mode-map
    (kbd \"C-c C-s\")             'my-save-command))
"
  (if (evenp (length binding-forms))
    (let* ((binding-pairs (seq-partition binding-forms 2))
           (mk-defkey (lambda (binding-pair) `(define-key ,keymap ,@binding-pair)))
           (defkey-forms (mapcar mk-defkey binding-pairs)))
      `(progn ,@defkey-forms))
    (error "`define-keys' requires an even number of binding forms.")))

(defmacro lambdai (args &rest body)
  "Creates an interactive lambda. Equivalent to:
(lambda ARGS (interactive) BODY)

For example:

(lambdai (x \"nX: \")
  (print (+ x 1)
  (print (+ x 2)
  (print (+ x 3)
  (+ x 3)))))

... is equivalent to:

(lambda (x)
  (interactive \"nX: \")
  (progn 
    (print (+ x 1)
    (print (+ x 2)
    (print (+ x 3)
    (+x 3))))))"
  (cond
   ((not args)   ;; no args provided
    `(lambda () (interactive)
       (progn ,@body)))
   ((listp args) ;; args provided
    `(lambda ,(butlast args)
       (interactive ,@(last args))
       (progn ,@body)))
   (t            ;; malformed args
    `(error "Invalid arg-list `%s'" ,args))))

(defmacro lambdap (args &rest body)
  "Creates a side-effecting lambda. Equivalent to:
(lambda ARGS (progn BODY))

For example:

(lambdap (x)
  (print (+ x 1)
  (print (+ x 2)
  (print (+ x 3)
  (+ x 3)))))

... is equivalent to:

(lambda (x)
  (progn 
    (print (+ x 1)
    (print (+ x 2)
    (print (+ x 3)
    (+x 3))))))"
  `(lambda ,args
     (progn ,@body)))


(provide 'util)
